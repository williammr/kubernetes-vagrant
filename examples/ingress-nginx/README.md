# NGINX Controller Installation

## Add NGINX Helm respository

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm list
helm search repo ingress
```

## Download NGINX chart values

```
helm show values ingress-nginx/ingress-nginx > /tmp/ingress-nginx.yaml
```

## Modify Helm NGINX values

```
vim /tmp/ingress-nginx.yaml

In the controller section search for following entries and change accordingly:

- hostNetwork: true

- hostPort:
    enabled: true

- kind: DaemonSet
```

## Install NGINX Controller

```
kubectl create ns ingress-nginx
helm install ingress-controller ingress-nginx/ingress-nginx -n ingress-nginx --values /tmp/ingress-nginx.yaml
```

## Validate NGINX Controller configuration

```
helm list -n ingress-nginx
NAME              	NAMESPACE    	REVISION	UPDATED                                	STATUS  	CHART              	APP VERSION
ingress-controller	ingress-nginx	1       	2021-10-21 12:54:41.142305361 +0000 UTC	deployed	ingress-nginx-4.0.6	1.0.4

kubectl -n ingress-nginx get all

NAME                                                    READY   STATUS    RESTARTS   AGE
pod/ingress-controller-ingress-nginx-controller-7mj68   1/1     Running   0          2m13s
pod/ingress-controller-ingress-nginx-controller-t7czf   1/1     Running   0          5m34s

NAME                                                            TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)                      AGE
service/ingress-controller-ingress-nginx-controller             LoadBalancer   10.100.255.143   192.168.56.201   80:31613/TCP,443:31173/TCP   5m34s
service/ingress-controller-ingress-nginx-controller-admission   ClusterIP      10.108.166.130   <none>           443/TCP                      5m34s

NAME                                                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/ingress-controller-ingress-nginx-controller   2         2         2       2            2           kubernetes.io/os=linux   5m34s
```

