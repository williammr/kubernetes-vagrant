#!/usr/local/bin

# deploy services
kubectl apply -f service-main.yaml
kubectl apply -f service-blue.yaml
kubectl apply -f service-green.yaml

# deploy deployments
kubectl apply -f nginx-deploy-main.yaml
kubectl apply -f nginx-deploy-blue.yaml
kubectl apply -f nginx-deploy-green.yaml

# deploy ingresses
kubectl apply -f ingress-resource-2.yaml
