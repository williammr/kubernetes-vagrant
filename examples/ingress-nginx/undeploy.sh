#!/usr/local/bin

# deploy ingresses
kubectl delete -f ingress-resource-2.yaml

# deploy deployments
kubectl delete -f nginx-deploy-main.yaml
kubectl delete -f nginx-deploy-blue.yaml
kubectl delete -f nginx-deploy-green.yaml

# deploy services
kubectl delete -f service-main.yaml
kubectl delete -f service-blue.yaml
kubectl delete -f service-green.yaml


