
# Vagrantfile and Scripts to Automate Kubernetes Setup using Kubeadm [Practice Environment for CKA/CKAD and CKS Exams]

## Personal environment

- VirtualBox Version 6.1.26 r145957 (Qt5.15.2)
- Debian 11 Bullseye 
- Vagrant 2.2.14
- bento/ubuntu-20.04
- Kubectl v1.22.0-00
- Kubernetes v1.22.0-00 (2021-Oct-09)

## Resources added

- [MetalLB](https://metallb.universe.tf/installation/#installation-by-manifest)
- [Ingress Controller](https://kubernetes.github.io/ingress-nginx/deploy/#using-helm)
- [Istio](https://istio.io/latest/docs/setup/getting-started/)

## Access Grafana

Get the Grafana LoadBalancer IP Address

```
$ kubectl get services -n prometheus
```

```
Username: admin
Password: prom-operator
```

## Documentation - Official Project

Refer this link for documentation: https://devopscube.com/kubernetes-cluster-vagrant/

If you are preparing for CKA, CKAD or CKS exam, save $57 using code **DCUBEOFFER** at https://kube.promo/latest

## Prerequisites

1. Working Vagrant setup
2. 8 Gig + RAM workstation as the Vms use 3 vCPUS and 4+ GB RAM
 
## Usage/Examples

To provision the cluster, execute the following commands.

```shell
git clone https://github.com/scriptcamp/vagrant-kubeadm-kubernetes.git
cd vagrant-kubeadm-kubernetes
vagrant up
```

## Set Kubeconfig file varaible.

```shell
cd vagrant-kubeadm-kubernetes
cd configs
export KUBECONFIG=$(PWD)/config
```

or you can copy the config file to .kube directory.

```shell
cp config ~/.kube/
```

## Kubernetes Dashboard URL

```shell
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=kubernetes-dashboard
```

## Kubernetes login token

Vagrant up will create the admin user token and saves in the configs directory.

```shell
cd vagrant-kubeadm-kubernetes
cd configs
cat token
```

## To shutdown the cluster, 

```shell
vagrant halt
```

## To restart the cluster,

```shell
vagrant up
```

## To destroy the cluster, 

```shell
vagrant destroy -f
```

## Centos & HA based Setup

If you want Centos based setup, please refer https://github.com/marthanda93/VAAS

## How to deploy NGINX Ingress Controller  

[NGINX Ingress Controller](https://www.youtube.com/watch?v=UvwtALIb2U8)
