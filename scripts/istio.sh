#! /bin/bash

# install istioctl
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.11.3 TARGET_ARCH=x86_64 sh -

# add istioctl to path
echo 'export PATH=$PATH:/home/vagrant/istio-1.11.3/bin' >> /home/vagrant/.profile

# source the profile
source /home/vagrant/.profile

# set kubeconfig
cd /home/vagrant/.kube && export KUBECONFIG=$(pwd)/config

# install istio
istioctl install --set profile=demo -y

# add a namespace label to instruct istio to automatically inject envoy sidecar proxies
kubectl label namespace default istio-injection=enabled

# install kiali
cd /home/vagrant/istio-1.11.3 && kubectl apply -f samples/addons