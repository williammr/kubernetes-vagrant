#! /bin/bash

/bin/bash /vagrant/configs/join.sh -v

sudo -i -u vagrant bash << EOF
mkdir -p /home/vagrant/.kube
sudo cp -i /vagrant/configs/config /home/vagrant/.kube/
sudo chown 1000:1000 /home/vagrant/.kube/config
sudo mkdir -p /home/root/.kube
sudo cp -i /vagrant/configs/config /root/.kube/
sudo chown root:root /root/.kube/config
NODENAME=$(hostname -s)
kubectl label node $(hostname -s) node-role.kubernetes.io/worker=worker-new
echo 'alias k=kubectl' >> /home/vagrant/.profile
EOF

NODENAME=$(hostname -s)
if [ ${NODENAME} == "worker-node02" ]; then
  /bin/bash /vagrant/scripts/helm.sh
fi
