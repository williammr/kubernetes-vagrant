#! /bin/bash

export KUBECONFIG=/home/vagrant/.kube/config

echo "adding helm repositories"
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
helm repo update

echo "installing ingress nginx"
helm show values ingress-nginx/ingress-nginx > /tmp/ingress-nginx2.yaml

sed -i 's/hostNetwork: false/hostNetwork: true/g' /tmp/ingress-nginx2.yaml
sed -i 's/kind: Deployment/kind: DaemonSet/g' /tmp/ingress-nginx2.yaml
awk -v new="true" 'prev=="hostPort:"{sub(/false/,""); $0=$0 new} {prev=$1} 1' /tmp/ingress-nginx2.yaml > /tmp/ingress-nginx.yaml
	
kubectl create ns ingress-nginx
helm install ingress-controller ingress-nginx/ingress-nginx -n ingress-nginx --values /tmp/ingress-nginx.yaml

echo "installing prometheus and grafana"
kubectl create ns prometheus
helm install prometheus prometheus-community/kube-prometheus-stack --set grafana.service.type=LoadBalancer -n prometheus

echo "installing nfs-subdir-external-provisioner"
kubectl create ns nfs-external
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=192.168.56.13 --set nfs.path=/srv/nfs/kubedata -n nfs-external

echo "validate helm installation"
helm list -n ingress-nginx
helm list -n prometheus
helm list -n nfs-external 
